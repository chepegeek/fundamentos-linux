## Acerca de
Este repositorio es publico y forma parte del curso **Fundamentos de GNU/Linux** al cual puedes acceder desde este enlace: [Fundamentos de GNU/Linux](https://www.youtube.com/playlist?list=PLuyqRxNKadznm0l3q2NFT9Xo6Q26Kty1e).
<br>

Las instrucciones sobre:
- Como descargar este repositorio.
- Como mantener mis archivos sincronizados con el repositorio.

se describen a continuacion.<br>

### Pre-requisitos
Debes tener instalado el sistema de control de versiones **Git** en tu equipo, si no es asi, puedes instalarlo ejecutando las siguientes instrucciones en tu terminal o consola:<br>

##### Instalacion en Debian
```
$ su

# apt-get install libcurl4-gnutls-dev libexpat1-dev gettext libz-dev libssl-dev

# apt-get install git
```

##### Instalacion en Ubuntu/Xubuntu/Kubuntu
```
$ sudo su

# apt-get install libcurl4-gnutls-dev libexpat1-dev gettext libz-dev libssl-dev

# apt-get install git
```

### ¿Como descargo los archivos de este repositorio a mi equipo?

Pasos a seguir:
1. Abre la terminal o consola de tu sistema.
2. Ubicate con el comando *$ cd* en la carpeta donde quieres descargar el repositorio.
3. Ejecuta en tu terminal la siguiente instruccion:
```
$ git clone https://gitlab.com/chepegeek/fundamentos-linux.git
```

¡Listo!<br>

### ¿Como mantengo mis archivos y carpetas actualizados con el repositorio?
Para tener la ultima version del repositorio en tu equipo debes hacer lo siguiente:

1. Abre la terminal o consola de tu sistema.
2. Ubicate con el comando *$ cd* en la carpeta donde descargaste este repositorio por primera vez.
3. Ejecuta en tu terminal las siguientes instrucciones:
```
$ git fetch origin
$ git merge origin/master
```

¡Listo!<br>